const express = require('express');
const data = require('./Data/users.json');
const bodyParser = require('body-parser');
const app = express();

//variables globales
const global = require('./global');

//controllers
const user_controller = require("./Controllers/users_controller.js");
const auth_controller = require('./Controllers/auth_controller');
const account_controller = require("./Controllers/account_controller");
const transactions_controller = require("./Controllers/transactions_controller");

app.use(bodyParser.json());

//GET Users with ID
app.get(global.URL_BASE + "users/:id", user_controller.getById);


app.get(global.URL_BASE + "users/:id/accounts",account_controller.getAccounts);

app.get(global.URL_BASE + "users/:id/accounts/:idd", account_controller.getById);

app.get(global.URL_BASE + "users/:id/accounts/:idd/transactions", transactions_controller.getTransaction);

app.get(global.URL_BASE + "users/:id/accounts/:idd/transactions/:iddd", transactions_controller.getById);

//GET Users with querys
//app.get(global.URL_BASE + "users", user_controller.getQuerys);

//GET Users List
app.get(global.URL_BASE + "users", user_controller.getUsers);

app.post(global.URL_BASE + "users", user_controller.postUsers);

//PUT users
app.put(global.URL_BASE + "users/:id", user_controller.putUser);

//DELETE users
app.delete(global.URL_BASE + "users/:id", user_controller.deleteUser);

//LOGIN users
app.post(global.URL_BASE + "users/login", auth_controller.Login);

//LOGOUT users
app.post(global.URL_BASE + "users/:id", auth_controller.Logout);

//Listen the port: 3000
app.listen(global.port, function() {
  console.log("Example app listening on port 3006!");
});
