const global = require('../global');
const data = require('../Data/users.json');
const bodyParser = require('body-parser');
require('dotenv').config();


// Petición PUT login user con id de mLab (_id.$oid)
function Login (req, res) {
  console.log('llego al controlador, metodo putLoginUserByEmail()');
  var httpClient = global.requestJSON.createClient(process.env.dbMlabURL);
  var email = req.body.email;
  var password = req.body.password;
  var queryString = 'q={"email": "' + email+ '", "password": "' + password + '"}&';
  let newProperty= '{"$set":{"logged":true}}';
  console.log('users?' + queryString + global.apiKey);
  console.log("Cambio: " + newProperty);
  httpClient.put('users?' + queryString + global.apiKey, JSON.parse(newProperty),
    function(errPut, resPut, bodyPut) {
      console.log('bodyPut.n: ' + bodyPut.n);
      if(errPut) {
          response = {"msg" : "Error en el logueo del usuario."}
      } else {
        if (bodyPut.n = 1) response = {"msg" : "Logueo exitoso."}
        else response = {"msg" : "No se encontrò al usuario."}
      }
      res.send(response);
    });
  }


//LOGOUT users
function Logout(req, res) {
  console.log("Logout");
  console.log(req.params.id);
  console.log(req.body.email);
  console.log(req.body.password);
  let flag_exits = false;
  let idUsuario = 0;
  let pos = req.params.id - 1;
  if (data[pos].logged == true) {
    data[pos].logged = false;
    idUsuario = data[pos];
    flag_exits = true;
  }
  if (flag_exits) {
    //fue encontrado
    res.send({
      "Logout Success": "Sì",
      id: data[pos - 1]
    });
  } else {
    res.send({
      "User not Login": "401"
    });
  }
}
//function for t write in Json
function writeFile(data) {
  const fs = require("fs");
  var jsonData = JSON.stringify(data);
  fs.exists('../Data/userLog.json', function(exists){
    if(exists){
      console.log("File exists!");
      fs.readFile('../Data/userLog.json', function readFileCallback(err, jsonData){
        if(err){
          console.log(err);
        }else{
          fs.writeFile('../Data/userLog.json', jsonData, "utf8", function (err){
            if(err){
              console.log(err);
            }else{
              console.log("JSON Generated!");
            }
          })
        }
      })
    }else{
      console.log("File not exists!");
      fs.writeFile('../Data/userLog.json', jsonData, "utf8", function(err){
        if(err){
          console.log(err);
        }else{
          console.log("JSON Generated!");
        }
      })
    }
  })
}

//Elements of the object Module
module.exports = {
  Login,
  Logout
}