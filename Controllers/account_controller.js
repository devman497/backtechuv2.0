const data = require('../Data/users.json');
const global = require('../global');
const requestJSON = require('request-json');

//GET Users List
function getAccounts(req, res) {
  var httpClient = requestJSON.createClient(global.dbMlabURL);
  var id = req.params.id;
  console.log(id);
  console.log("Client HTTP mlab created!");
  //Query mLab
  var sQuery = 'q={id_user:'+ id+ '}&f={_id:0}&';
  httpClient.get('accounts?'+ sQuery + global.apiKey,
    function(err, resMlab, body){
      var response = {};
      if(err){
        response = {"msg": "Error obteniendo Users"}
        res.status(500);
      }else{
        if(body.length>0){
          console.log(body.length);
          response= body;
        }else{
          response = { "msg": "Ningun user obtenido"}
          res.status(404);
        }
      }
      res.send(response);
    });
}

//GET Users by ID
function getById(req, res) {
  var httpClient = requestJSON.createClient(global.dbMlabURL);
  console.log("Client HTTP mlab created!");
  var id = req.params.id;
  var idd = req.params.idd;
  console.log("idd: " + idd);
  //var squeryfield = 'q={"id":'+ id+ '}&'
  //Query mLab
  var sQuery = 'q={id_user:'+ id +', id:'+ idd + '}&';
  httpClient.get('accounts?'+ sQuery + global.apiKey,
    function(err, resMlab, body){
      var response = {};
      if(err){
        response = {"msg": "Error obteniendo Users"}
        res.status(500);
      }else{
        if(body.length>0){
          console.log(body.length);
          response= body;
        }else{
          response = { "msg": "Not exists this account!"}
          res.status(404);
        }
      }
      res.send(response);
    });
}

// function putUser(req, res) {
//   let editUser = {
//     id: req.params.id,
//     first_name: req.body.first_name,
//     last_name: req.body.last_name,
//     email: req.body.email,
//     password: req.body.password
//   };
//   data[req.params.id - 1] = editUser;
//   console.log("User Edited!: " + editUser);
//   res.send(editUser);
// }
// //DELETE User
// function deleteUser(req, res) {
//   let pos = req.params.id;
//   data.splice(pos - 1, 1);
//   res.send({
//     msg: "User deleted!"
//   });
// }
// // POST users
// function postUser(req, res){
//   console.log("POST");
//   console.log(req.body.first_name);
//   console.log(req.body.email);

//   let newPos = data.length + 1;
//   let newUser = {
//     "id": newPos,
//     "first_name": req.body.first_name,
//     "last_name": req.body.last_name,
//     "email": req.body.email,
//     "password": req.body.password
//   }
//   data.push(newUser);
//   res.send(newUser);
//   console.log(newPos);
// }


//exports to the object module
module.exports = {
  getAccounts,
  getById
}
