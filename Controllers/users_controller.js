const data = require('../Data/users.json');
const global = require('../global');
const requestJSON = require('request-json');

//GET Users with Querys
function getQuerys(req, res) {
  var httpClient = requestJSON.createClient(global.dbMlabURL);
  console.log("Client HTTP mlab created!");
  //Query mLab
  var sQuery = 'f={"first_name":1, "last_name":1, "_id":0}&l=10&s={"id":1}&';
  httpClient.get('users?'+ sQuery + global.apiKey,
    function(err, resMlab, body){
      var response = {};
      if(err){
        response = {"msg": "Error obteniendo Users"}
        res.status(500);
      }else{
        if(body.length>0){
          console.log(body.length);
          response= body;
        }else{
          response = { "msg": "Ningun user obtenido"}
          res.status(404);
        }
      }
      res.send(response);
    });
}

//GET Users List
function getUsers(req, res) {
  var httpClient = requestJSON.createClient(global.dbMlabURL);
  console.log("Client HTTP mlab created!");
  //Query mLab
  var sQuery = 'f={"_id":0}&';
  httpClient.get('users?'+ sQuery + global.apiKey,
    function(err, resMlab, body){
      var response = {};
      if(err){
        response = {"msg": "Error obteniendo Users"}
        res.status(500);
      }else{
        if(body.length>0){
          console.log(body.length);
          response= body;
        }else{
          response = { "msg": "Ningun user obtenido"}
          res.status(404);
        }
      }
      res.send(response);
    });
}

//GET Users by ID
function getById(req, res) {
  var httpClient = requestJSON.createClient(global.dbMlabURL);
  console.log("Client HTTP mlab created!");
  var id = req.params.id;

  var squeryfield = 'q={"id":'+ id+ '}&f={_id:0}&'
  //Query mLab
  //var sQuery = 'f={"first_name":1, "last_name":1, "_id":0}&l=10&s={"id":1}&';
  httpClient.get('users?'+ squeryfield + global.apiKey,
    function(err, resMlab, body){
      var response = {};
      if(err){
        response = {"msg": "Error obteniendo Users"}
        res.status(500);
      }else{
        if(body.length>0){
          console.log(body.length);
          response= body;
        }else{
          response = { "msg": "Ningun user obtenido"}
          res.status(404);
        }
      }
      res.send(response);
    });
}

// Petición PUT con id de mLab (_id.$oid)
function putUser(req, res) {
  var id = req.params.id;
  let userBody = req.body;
  var queryString = 'q={"id":' + id + '}&';
  var httpClient = requestJSON.createClient(global.dbMlabURL);
  httpClient.get('users?' + queryString + global.apiKey,
    function(err, respuestaMLab, body){
      let response = body[0];
      console.log(body);
      //Actualizo campos del usuario
      let updatedUser = {
        "id" : req.body.id,
        "first_name" : req.body.first_name,
        "last_name" : req.body.last_name,
        "email" : req.body.email,
        "password" : req.body.password
      };//Otra forma simplificada (para muchas propiedades)
      // var updatedUser = {};
      // Object.keys(response).forEach(key => updatedUser[key] = response[key]);
      // Object.keys(userBody).forEach(key => updatedUser[key] = userBody[key]);          // PUT a mLab
      httpClient.put('users/' + response._id.$oid + '?' + global.apiKey, updatedUser,
        function(err, respuestaMLab, body){
          var response = {};
          if(err) {
              response = {
                "msg" : "Error actualizando usuario."
              }
              res.status(500);
          } else {
            if(body.length > 0) {
              response = body;
            } else {
              response = {
                "msg" : "Usuario actualizado correctamente."
              }
              res.status(200);
            }
          }
          res.send(response);
        });
    });
}


//DELETE User
function deleteUser(req, res) {
  let pos = req.params.id;
  data.splice(pos - 1, 1);
  res.send({
    msg: "User deleted!"
  });
}
// POST users
function postUsers(req, res){
  var clientMlab =requestJSON.createClient(global.dbMlabURL);
  clientMlab.get('users?' + global.apiKey,
    function(err, mLab, body){
      var newID = body.length +1;
      let newUser = {
        "id": newID,
        "first_name": req.body.first_name,
        "last_name": req.body.last_name,
        "email": req.body.email,
        "password": req.body.password
      };
      clientMlab.post('users?'+ global.apiKey, newUser,
        function(err, resMlab, bodi){
          res.send(bodi);
        })
    });
}


//exports to the object module
module.exports = {
  getQuerys,
  getById,
  getUsers,
  postUsers,
  putUser,
  deleteUser
}
