#Image Docker base/inicial
FROM node:latest

#Crear el directorio del contenedor Docker
WORKDIR /docker-apitechu

#Copie los archivos del proyecto en el directorio del contenedor
ADD . /docker-apitechu


#Exponer el puerto de escucha del contenedor (el mismo que nuestra api)
EXPOSE 3006


#Lanzar comandos parra ejecutar nuestra app (cada palabra del comando bajo comillas)
CMD ["npm", "run", "prod"]